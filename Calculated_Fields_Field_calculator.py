##Area Of Veg Score
##Pre Logic Scripting Model Area of Veg Score
## Max Score of 10 
def Reclass(Sum_Inter):
	if (Sum_Inter < 20):
		return 1
	elif (Sum_Inter >= 20 and Sum_Inter <50):
		return 2
	elif (Sum_Inter >= 50 and Sum_Inter <100):
		return 3
	elif (Sum_Inter >= 100 and Sum_Inter <200):
		return 4
	elif (Sum_Inter >= 200 and Sum_Inter <500):
		return 5
	elif (Sum_Inter >= 500 and Sum_Inter <1000):
		return 6
	elif (Sum_Inter >= 1000):
		return 10

 
####Make sure you reclass the area field that you are calculating from 
Reclass( !Area!)

## Pre Logic Scripting Percentage of Veg Score
##Max Score of 4

def Reclass(Percentage_Score):
	if(Percentage_Score < 25):
		return 1
	elif(Percentage_Score >= 25 and Percentage_Score < 50):
		return 2
	elif(Percentage_Score >= 50 and Percentage_Score < 75):
		return 3
	elif(Percentage_Score >= 75):
		return 4	

Reclass( !PERCENTAGE!)

##pre logic for Near distance calculation 
##Max Score of 5
def Reclass(Distance_Score):
	if(Distance_Score < 1000):
		return 5
	elif(Distance_Score >=1000 and Distance_Score <2500):
		return 3
	elif(Distance_Score >=2500 and Distance_Score <5000):
		return 2
	elif(Distance_Score >5000):
		return 1

Reclass(!NEAR_DIST!)
