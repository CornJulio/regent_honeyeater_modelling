Documentation of Regent Honeyeater Modelling project 


Layers Required:
PCT Layer 
Holdings Layer 
Regent Honeyeater Sites (NRHMP and Public Sitings) 

Create File Geodatabase 
Create Feature Dataset "Data_Projected", Dependent on zone choose appropriate Projected coordinate system

Spatial Selection - Select Holdings that intersect Vegetation.  Import into "Data_Projected" Save as "Holdings_Intersected"
Import PCT Layer into "Data Projected"

Open "Advanced Licsense " of ARC
Open "Tabulate Intersection (Analysis)" tool
	Input Zone Features = "Holdings_Intersected"
		Zone Fields = "Holdings_Reference_Number"
	Input Class Features = PCT Layer
	Output Table = "\Root_FGDB\Intersected_Table"
	### can calclulate based on PCT TYPE etc but no need to###


Complete Join: "Holdings_Interescted" with "Intersected Table" based on Holdings Reference Number
Export data into "Data Projected" 
File Name: "Holdings Join Tabulated"
Calculate_Geometery on "Area" from sq metres to Ha in  ""Holdings Join Tabulated""
###Delete Uneeded Fields - I usually just Leave Holdings Reference Number Rateable Area Area Percentage #####

Layer to use "Holdings Join Tabulated"
Add Field "Area of Veg Score" Type Integer

Calculate a score  based on the Area of Vegetation on Property using the Field calculator (Using python calculator)

##Area Of Veg Score
##Pre Logic Scripting Model Area of Veg Score
## Max Score of 10 
def Reclass(Sum_Inter):
	if (Sum_Inter < 20):
		return 1
	elif (Sum_Inter >= 20 and Sum_Inter <50):
		return 2
	elif (Sum_Inter >= 50 and Sum_Inter <100):
		return 3
	elif (Sum_Inter >= 100 and Sum_Inter <200):
		return 4
	elif (Sum_Inter >= 200 and Sum_Inter <500):
		return 5
	elif (Sum_Inter >= 500 and Sum_Inter <1000):
		return 6
	elif (Sum_Inter >= 1000):
		return 10

	

Area of Veg Score 
Paste  into Box ####Make sure you reclass the area field that you are calculating from 
Reclass( !Area!)


Add Field "Percentage_Score", Type "Integer"

Calculate a score  based on the Area of Vegetation on Property using the Field calculator (Using python calculator)

## Pre Logic Scripting Percentage of Veg Score
##Max Score of 4

def Reclass(Percentage_Score):
	if(Percentage_Score < 25):
		return 1
	elif(Percentage_Score >= 25 and Percentage_Score < 50):
		return 2
	elif(Percentage_Score >= 50 and Percentage_Score < 75):
		return 3
	elif(Percentage_Score >= 75):
		return 4	

	
##Paste  into Box
Reclass( !PERCENTAGE!)

open Near Analysis Tool:
	Input Features = "Holdings Join Tabulated"
	Near Features = "Regent Honeyeater Sites"

##this will add the distance from Features to your Layer##

Add Field "Near_distance_calculation" Type Integer


Calculate a score  based on theNear Distance Value using the Field calculator (Using python calculator)

##pre logic for Near distance calculation 
##Max Score of 5
def Reclass(Distance_Score):
	if(Distance_Score < 1000):
		return 5
	elif(Distance_Score >=1000 and Distance_Score <2500):
		return 3
	elif(Distance_Score >=2500 and Distance_Score <5000):
		return 2
	elif(Distance_Score >5000):
		return 1

Reclass(!NEAR_DIST!)

Add Field "Total Score", Type Integer
Use Field Caculator to Add - "Near_distance_calculation" + "Percentage_Score" + "Area of Veg Score"